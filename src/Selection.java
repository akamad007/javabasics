
public class Selection {
	public static void main(String [] args){
		
		Integer[] some = {3,4,8,1,2,9,10};
		for (int k =0;k<some.length;k++){
			System.out.print(some[k]+" ");	
		}
		
		for(int i=0;i<some.length-1;i++){
			int min = i;
			for(int j = i+1;j<some.length;j++){
				if(some[j]<some[min]){
					min = j;
				}				
			}
			
			some = swap(some,i,min);
		}
		for (int k =0;k<some.length;k++){
			System.out.print(some[k]+" ");	
		}
	}
	public static Integer [] swap (Integer[] some, int i, int j)
	{
	    int temp = some[i];
	    some[i] = some[j];
	    some[j] = temp;
	    return some;
	}
}
