
class Singleton{
	private static Singleton sin = new Singleton();
	private Singleton(){}
	public static Singleton create(){
		return sin;
	}
}

public class AkaSingleton {
	public static void main(String [] args){
		Singleton  single = Singleton.create();
		
	}
}
