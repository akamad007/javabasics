import java.util.*;

class AkaNode {
	
	private int data ;
	public AkaNode pointer = null;
	public void set(AkaNode node){
		pointer = node;
	}
	public AkaNode get(){
		return pointer;
	}
	public void pPrint(){
		System.out.println(data);
	}
	public void setData(int data){
		
		this.data = data;
		
	}
public int getData(){
		
		return this.data;
		
	}
}
class AkaList implements Iterator{
	private int size = 0;
	private AkaNode last = null;
	private AkaNode head = null;
	public AkaNode current = null;
	public boolean add(int data){
		if (last == null)
		{
			AkaNode some = new AkaNode();
			some.setData(data);
			head = some ;
			last = some;	
			current = some;
		}
		else{
			last.set(new AkaNode());			
			last = last.get();
			last.setData(data);
		}
			
		size = size+1;
		return true;
	}
	public void reset(){
		current = head;
	}
	public int delete(){
		int data = -1;
		AkaNode prev = null;
		this.reset();
		 prev = this.next();		
		 if (prev!=null){
				if (last==head){
					data = prev.getData();
					prev = null;
					head = prev;
					current = prev;
					
					
				}else{
					while(this.hasNext()){	
						
						 if (prev.pointer==last)						
							 break;
						 prev = this.next();
					}
					data = prev.get().getData();
					prev.set(null);
				}
				
				last = prev;
				size = size -1;
				return data;
		}
		else{
				 return -1;
		}
	}
	public int size(){
		return size;
	}
	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		
		if(current!=null)
			return true;
		else
			return false;
		
	}

	@Override
	public AkaNode next() {
		// TODO Auto-generated method stub
		if(current==null)
			return null;
		else
			{
			AkaNode n = current.get();
			AkaNode some = current;
			
			current = n;
			return some;
			}
		
	}
	
	
}

public class AkaLinkedList {
	public static void main(String [] args){
		AkaList a = new AkaList();
		a.add(99);
		
		a.add(45);
		
		a.add(88);
		
		
		while(a.hasNext()){
			a.next().pPrint();
		}
		a.reset();
		a.delete();
		a.delete();
		a.delete();
		System.out.println(a.hasNext());
		
		a.add(45);
		a.add(34);
		while(a.hasNext()){
			a.next().pPrint();
		}
		a.delete();
		while(a.hasNext()){
			a.next().pPrint();
		}
		a.delete();
		a.delete();
		while(a.hasNext()){
			a.next().pPrint();
		}
	}
	
}
