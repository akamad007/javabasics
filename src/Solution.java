import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    static long minDifference = -1;
    private static long findTheMin(long candies[], long K ){
    	 long someValue = 0;
    	
        for(int i =0;i<candies.length-K-1;i++){
        	someValue = candies[(int) (i+K-1)] - candies[i] ;        	
        	if(minDifference==-1||minDifference>someValue){
                minDifference = someValue;
            }
        }
        return minDifference;
    }
    
    public static void main(String[] args) {
       Scanner sc = new Scanner(System.in);
        long totalN = sc.nextLong();
        long totalK = sc.nextLong();
        long max = 0;
        long min = 0;
        long candies[] = new long [(int)totalN];
        for(int i=0;i<totalN;i++){
            if(sc.hasNextLong()){
                candies[i] = sc.nextLong();
            }
        }
    	Arrays.sort(candies);
    	findTheMin(candies,totalK);
        System.out.println(minDifference);
        
    }
}